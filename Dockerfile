FROM    python:3.11.5
WORKDIR  /app
COPY . /app
RUN  pip install fastapi uvicorn
RUN pip install alembic
EXPOSE 8000
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8002"]
