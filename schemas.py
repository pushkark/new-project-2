from pydantic import BaseModel
# from typing import List
class DepartmentBase(BaseModel):

    name: str

class DepartmentCreate(DepartmentBase):
    pass

class Department(DepartmentBase):
    id: int

    class Config:
        orm_mode = True

class InstructorBase(BaseModel):
    name: str

class InstructorCreate(InstructorBase):
    department_id: int

class Instructor(InstructorBase):
    id: int
    department: Department

    class Config:
        orm_mode = True

class CourseBase(BaseModel):
    name: str
    department_id: int
    instructor_id: int

class CourseCreate(CourseBase):
    pass

class Course(CourseBase):
    id: int
    department: Department
    instructor: Instructor

    class Config:
        orm_mode = True

class StudentBase(BaseModel):
    name: str

class StudentCreate(StudentBase):
    pass

class Student(StudentBase):
    id: int

    class Config:
        orm_mode = True

class EnrollmentBase(BaseModel):
    student_id: int
    course_id: int

class EnrollmentCreate(EnrollmentBase):
    pass

class Enrollment(EnrollmentBase):
    id: int
    student: Student
    course: Course

    class Config:
        orm_mode = True
