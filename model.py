from typing import List

from sqlalchemy import create_engine, ForeignKey, Table
from sqlalchemy.engine import URL
from sqlalchemy.orm import declarative_base, sessionmaker, mapped_column, relationship, Mapped
from sqlalchemy import Column

DATABASE_URL = URL.create(
    drivername="postgresql",
    username="postgres",
    password="postgres",
    host="localhost",
    database="postgres",
    port="5432"
)

Base = declarative_base()
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

class Department(Base):
    __tablename__ = "departments"
    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    name: Mapped[str] = mapped_column(unique=True, index=True)
    instructors: Mapped[List["Instructor"]] = relationship(back_populates="department")
    courses: Mapped[List["Course"]] = relationship(back_populates="department")

class Instructor(Base):
    __tablename__ = "instructors"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    name: Mapped[str] = mapped_column(index=True)
    department_id: Mapped[int] = mapped_column(ForeignKey("departments.id"))

    department: Mapped["Department"] = relationship(back_populates="instructors")

    courses: Mapped[List["Course"]] = relationship(back_populates="instructor")

association_table = Table(
    "Enrollment",
    Base.metadata,
    Column("course_id", ForeignKey("courses.id"), primary_key=True),
    Column("student_id", ForeignKey("students.id"), primary_key=True),
)

class Course(Base):
    __tablename__ = "courses"
    id: Mapped[int] = mapped_column(index=True, primary_key=True)
    name: Mapped[str] = mapped_column()
    department_id: Mapped[int] = mapped_column(ForeignKey("departments.id"))
    instructor_id: Mapped[int] = mapped_column(ForeignKey("instructors.id"))
    department: Mapped[Department] = relationship(back_populates="courses")
    instructor: Mapped["Instructor"] = relationship(back_populates="courses")
    students: Mapped[List["Student"]] = relationship(
        secondary=association_table, back_populates="courses"
    )

class Student(Base):
    __tablename__ = "students"
    id: Mapped[int] = mapped_column(index=True, primary_key=True)
    name: Mapped[str] = mapped_column()
    courses: Mapped[List["Course"]] = relationship(
        secondary=association_table, back_populates="students"
    )

# Base.metadata.create_all(bind=engine)
